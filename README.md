# README #

### What is this repository for? ###

* This is the repository for my personal website. I have multiple features included such as stickies and a specialized loading screen

### Who do I talk to for more info? ###

* Taylor Parker (taylorparker787@yahoo.com)

### I want to use this. ###

* *Great! Go ahead and clone. The website requires a server to run/test (because php) but overall it is a simple, one page scrolling website. All necessary code is included and requires no specific framework.*