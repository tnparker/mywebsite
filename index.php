<html>
<head>

<title>
Welcome to Taylor Parker's Website
</title>
<meta charset='utf-8'>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">

<!--<link rel="shortcut icon" href="titleLogo.ico" />-->

<link rel="stylesheet" type="text/css" media="all" href="style.css" />
<link href='https://fonts.googleapis.com/css?family=Oswald|Dosis:500|Exo|Rock+Salt|BenchNine:400|Orbitron' rel='stylesheet' type='text/css'>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="jquery.sticky.js"></script>
<script type="text/javascript">


var imgArray = ['imgGram.jpg', 'vc2.jpg', 'vcVDay.png', 'oldweb.png', 'me1.png'];
$(document).ready(

	function(){

		setSize();
		$('.theImages').each(function(num){
			$(this).attr('src', 'imgs/' + imgArray[num]);
		})

		$('.pt1').css("background", "url('imgs/pt1.png') center center fixed");
		$('.pt2').css("background", "url('imgs/pt2.jpg') center center fixed");
		$('.pt3').css("background", "url('imgs/pt3.jpg') center center fixed");
		$('.pt4').css("background", "url('imgs/pt4.jpg') center center fixed");

		$('a[href*=#]').click(function() {
		    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
		    && location.hostname == this.hostname) {
		      var $target = $(this.hash);
		      $target = $target.length && $target
		      || $('[name=' + this.hash.slice(1) +']');
		      if ($target.length) {
		        var targetOffset = $target.offset().top;
		        $('html,body')
		        .animate({scrollTop: targetOffset}, 1500);
		       return false;
		      }
		    }

		});

		$("#theNav").sticky({topSpacing:0});
	});

// Width-based mobile toggle

		function setSize() {

			var orientation = window.orientation;
			    
			    switch(orientation) {
			      case 90: case -90:
			        orientation = 'landscape';
			      break;
			      default:
			        orientation = 'portrait';
			    }

			if(orientation === 'landscape'){
				window.isMobile = window.innerHeight <= 600;
				window.isTablet = window.innerHeight <= 997 && window.innerHeight > 600;
				window.isDesktop = window.innerHeight > 997;
			}
			else{
				window.isMobile = window.innerWidth <= 600;
				window.isTablet = window.innerWidth <= 997 && window.innerWidth > 600;
				window.isDesktop = window.innerWidth > 997;
			}
			$('body').toggleClass('mobile', window.isMobile);
			$('body').toggleClass('tablet', window.isTablet);
			$('body').toggleClass('desktop', window.isDesktop);
		
	}

	$(window).resize(setSize());
	$(window).load(function(){$('body').addClass('loaded'); $('#loadingText, .sp').hide();})
// End width-based mobile toggle

</script>

</head>


<body>

<div id="loadingText">LOADING...</div>
<div class="sp sp-wave"></div>
<div id="loader-wrapper">
    <div id="loader"></div>
 
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
 
</div>

<div class = "theHeader" id="home">	

	</br>
	<span class="descHeadFont">Cosmopolitan nerd. Amateur code expert.</span></br>
	<span class="nameHeadFont">TAYLOR PARKER<br><span style="font-size:25px">SHE / HER</span></span></br>
	<span class="estHeadFont">EST. 1996 &sect; CA</span></br>
	<a class = "menubtns" href="#home">HOME</a>
	<div class = "spacer"></div>
	<a class = "menubtns" href="#theNav">WORKS</a>
	<div class = "spacer"></div>
	<a class = "menubtns" href="#about">ABOUT</a>
	<!--<a class = "menubtns" href="#contact">CONTACT</a>-->
	</br>
	<a href="#theNav"><img src="imgs/downarrow.png" class = "headerDownArrow"/></a>
</div>

<div class = "navMenu" id="theNav">

		<div class="logo">TAYLOR PARKER<br></div>              
		  <nav class="menu">
		      <ul id="nav">
		          <li class="selected navbtns"><a href="#home">HOME</a></li>
		          <li class="navbtns"><a href="#works">WORKS</a></li>              
		          <li class="navbtns"><a href="#about">ABOUT</a></li>         
		          <!--<li class="navbtns"><a href="#contact">CONTACT</a></li>-->
		      </ul>
		  </nav>
</div>

	<div class="pt1">
		<div class="projectText">
			<div class="projectTitle">2ND SOURCE WIRE & CABLE</div>
			<br>
			<span class="projectDescription">2nd Source Wire & Cable is a distributer of wire and cable products throughout the United States. This website features a simple design to allow for
			maximum usability for current and potential customers across all demographics. This company has a variety of aerospace related material which is carefully organize to create a streamlined experience.</span>
			<br>
			<div style="border:dotted thin rgba(0,0,0,0.4); display:inline-block; margin-left:2em;padding: 0px 2% 1% 2%;">
				<a href="http://www.2ndsourcewire.com">2ndsourcewire .com</a><br>
				<span class="projectTech">Technologies: HTML, jQuery, CSS3, JavaScript</span><br>
			</div>

		</div>
		<a href="http://www.2ndsourcewire.com" rel="external"><img class="theImages" src=""/></a>
	</div>

	<div class="pt2">
		<a href="http://volunteer.ucla.edu" rel="external"><img class="theImages" style="float:left; margin-left:3%;" src=""/></a>
		<div class="projectText">
			<div class="projectTitle">UCLA Volunteer Center</div>
			<br>
			<span class="projectDescription">The UCLA Volunteer Center is an endless expanse of volunteer opportunities. The website interacts with Flickr and Google to view event photos and sign up for events and/or the newsletter. On a mobile scale, the website features a photo uploader which directly uploads a volunteer's images straight from
			their device to the Volunteer Center's Flickr. </span>
			<br>
			<div style="border:dotted thin rgba(0,0,0,0.4); display:inline-block; margin-left:2em;padding: 0px 2% 1% 2%;">
				<a href="http://www.volunteer.ucla.edu">volunteer .ucla .edu</a><br>
				<span class="projectTech">Technologies: HTML, jQuery, CSS3, JavaScript, AJAX, PHP, WordPress, JSON/P</span><br>
			</div>

		</div>
	</div>

	<div class="pt4">
		<div class="projectText">
			<div class="projectTitle">UCLA Volunteer Day</div>
			<br>
			<span class="projectDescription">The UCLA Volunteer Day is the largest new-student volunteer event in the entire nation. Over 7,000+ volunteers spread across LA to give back to the community. The website
			features an interactive map which shows each site individually as well as sharing more information about UCLA volunteerism.</span>
			<br>
			<div style="border:dotted thin rgba(0,0,0,0.4); display:inline-block; margin-left:2em;padding: 0px 2% 1% 2%;">
				<a href="http://www.volunteerday.ucla.edu">volunteerday .ucla .edu</a><br>
				<span class="projectTech">Technologies: HTML, jQuery, CSS3, JavaScript, SquareSpace, AJAX</span><br>
			</div>

		</div>
		<a href="http://volunteerday.ucla.edu" rel="external"><img class="theImages" src=""/></a>
	</div>

	<div class="pt3">
		<a href="http://www.gitastudents.com/~parkert/index.htm" rel="external"><img class="theImages" style="float:left; margin-left:3%;" src=""/></a>	
		<div class="projectText">
			<div class="projectTitle">High School Projects</div>
			<br>
			<span class="projectDescription">This website is a time capsule of my programming work from freshman to senior year of high school. Each year of my high school career/programming progress is documented by a new website filled with
			interactive games. Java projects not included on live site.</span>
			<br>
			<div style="border:dotted thin rgba(0,0,0,0.6); display:inline-block; margin-left:2em;padding: 0px 2% 1% 2%;">
				<a href="http://www.gitastudents.com/~parkert/index.htm">gitastudents .com/~parkert/</a><br>
				<span class="projectTech" style="padding-bottom:5px;">Technologies: HTML, HTML5, jQuery, CSS3, JavaScript, <br>Flash/ActionScript3, Visual Basic, Java </span><br>
			</div>

		</div>
	</div>

	<div class="about" id="about">
		<div class="projectText">
			<div class="projectTitle" style="font-size:40px; font-family:'Orbitron'; letter-spacing:5px; border-bottom:1px dotted; padding-bottom:2px;">TAYLOR PARKER</div><br>
			<div class="projectDescription">
				<ul class="aboutMeList">
					<li>2<span style="font-size: 80%; vertical-align: top;">nd</span> year Neuroscience student at UCLA.</li>
					<li>Aspiring pre-med that moonlights as a computer geek</li>
					<li>Over 6 years of programming experience</li>
					<li>Knows 10+ programming languages varying from basic to skilled</li>
					<li>Strong believer in the efficiency of list formatting and ecletic bullet choices<br><br></li>
					<li style="list-style-type:none;"><i>I am simply a 19 year old nerd trying to reconcile technology and biology.</i><br><br></li>
					<li style="list-style-type:none;"> <table>
								<tr>
									<td style="padding:5%;">
										<a href="https://www.linkedin.com/in/tnparker1"><img class="linkIn" src="imgs/linkedIn.png"/></a>
									</td>
									<td style="padding:5%;">
										<a href="https://bitbucket.org/tnparker/mywebsite/overview"><img class="bitbucket" src="imgs/bitbucket.png"/></a>
									</td>
									<td style="padding:5%;">
									<!--<div>Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a>             is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0">CC BY 3.0</a></div>-->
										<a href="imgs/TaylorParkersResume.pdf" target="_blank"><img class="resume" src="imgs/professional5.png"/></a>
									</td>
								</tr>
						</table>
					</li>
				</ul>
				
			</div>
		</div>
			<img class="theImages" id="mePic" style="margin-top:2%;" src=""/>
	</div>
	<!--<div class="contact" id="contact"></div>-->
</div>



</body>


</html>
